import tkinter as tk

class LinkedInNotificationsApp:
    def __init__(self, master):
        self.master = master
        self.master.title("LinkedIn Notifications")
        
        # Mock notifications data
        self.notifications = [
            "John Smith liked your post.",
            "Jane Doe commented on your post.",
            "You have a new connection request from Alex.",
            "Your post reached 100 likes!",
            "Reminder: Complete your profile.",
        ]
        
        self.create_widgets()
    
    def create_widgets(self):
        self.notification_label = tk.Label(self.master, text="Notifications:", font=("Helvetica", 14, "bold"))
        self.notification_label.grid(row=0, column=0, columnspan=2, padx=10, pady=10)
        
        self.notification_frame = tk.Frame(self.master)
        self.notification_frame.grid(row=1, column=0, columnspan=2, padx=10, pady=10, sticky="nsew")
        
        for notification in self.notifications:
            notification_container = tk.Frame(self.notification_frame, bd=1, relief=tk.RIDGE)
            notification_container.pack(fill=tk.X, padx=10, pady=5)
            notification_label = tk.Label(notification_container, text=notification, font=("Helvetica", 12))
            notification_label.pack(padx=5, pady=5, fill=tk.X)

        self.refresh_button = tk.Button(self.master, text="Refresh", command=self.refresh_notifications, font=("Helvetica", 12, "bold"), bg="#4CAF50", fg="white")
        self.refresh_button.grid(row=2, column=0, padx=10, pady=10, sticky="ew")
        
        self.close_button = tk.Button(self.master, text="Close", command=self.master.quit, font=("Helvetica", 12, "bold"), bg="#FF5733", fg="white")
        self.close_button.grid(row=2, column=1, padx=10, pady=10, sticky="ew")

        # Configure grid weights to make the frame flexible
        self.master.grid_rowconfigure(1, weight=1)
        self.master.grid_columnconfigure(0, weight=1)
        self.master.grid_columnconfigure(1, weight=1)
        self.master.grid_rowconfigure(2, weight=0)

    def refresh_notifications(self):
        # This function would refresh the notifications from LinkedIn (not implemented in this example)
        # For simplicity, we'll just reload the mock notifications
        for widget in self.notification_frame.winfo_children():
            widget.destroy()
        
        for notification in self.notifications:
            notification_container = tk.Frame(self.notification_frame, bd=1, relief=tk.RIDGE)
            notification_container.pack(fill=tk.X, padx=10, pady=5)
            notification_label = tk.Label(notification_container, text=notification, font=("Helvetica", 12))
            notification_label.pack(padx=5, pady=5, fill=tk.X)

def main():
    root = tk.Tk()
    app = LinkedInNotificationsApp(root)
    # Set minimum size for the window
    root.minsize(400, 200)
    root.mainloop()

if __name__ == "__main__":
    main()
