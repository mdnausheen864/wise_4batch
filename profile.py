import tkinter as tk
from tkinter import filedialog, messagebox

class ProfilePage(tk.Tk):
    def __init__(self):
        super().__init__()
        self.title("Profile Page")
        self.geometry("400x500")
        self.configure(bg="#ADD8E6")

        self.profile_pic_label = tk.Label(self, text="Profile Picture", width=20, height=5, relief=tk.RAISED, font=("Helvetica", 12), bg="#ffffff")
        self.profile_pic_label.pack(padx=10, pady=10)

        self.change_pic_button = tk.Button(self, text="Change Picture", command=self.change_profile_pic, bg="#4CAF50", fg="white", font=("Helvetica", 12))
        self.change_pic_button.pack(pady=5)

        self.profile_label = tk.Label(self, text="Profile Information", font=("Helvetica", 16, "bold"), bg="#ADD8E6")
        self.profile_label.pack(pady=(10, 5))

        self.labels = [
            ("Name: John Doe", "#333333"),
            ("Age: 30", "#333333"),
            ("Phone: 123-456-7890", "#333333"),
            ("Email: johndoe@example.com", "#333333"),
            ("Location: New York", "#333333")
        ]

        for label_text, color in self.labels:
            label = tk.Label(self, text=label_text, font=("Helvetica", 14), bg="#ADD8E6", fg=color)
            label.pack()

        self.change_password_button = tk.Button(self, text="Change Password", command=self.open_change_password_page, bg="#2196F3", fg="white", font=("Helvetica", 12))
        self.change_password_button.pack(pady=5)

        self.change_email_button = tk.Button(self, text="Change Email", command=self.open_change_email_page, bg="#2196F3", fg="white", font=("Helvetica", 12))
        self.change_email_button.pack(pady=5)

        self.change_mobile_button = tk.Button(self, text="Change Mobile Number", command=self.open_change_mobile_page, bg="#2196F3", fg="white", font=("Helvetica", 12))
        self.change_mobile_button.pack(pady=5)

        self.photo = None  # Reference to the image object

    def change_profile_pic(self):
        file_path = filedialog.askopenfilename(filetypes=[("Image files", "*.png;*.jpg;*.jpeg")])
        if file_path:
            self.display_profile_pic(file_path)

    def display_profile_pic(self, file_path):
        if self.photo:
            self.profile_pic_label.config(image='')  # Clear previous image
        self.photo = tk.PhotoImage(file=file_path)
        self.profile_pic_label.config(image=self.photo)

    def open_change_password_page(self):
        self.change_password_window = ChangePasswordPage(self)

    def open_change_email_page(self):
        self.change_email_window = ChangeEmailPage(self)

    def open_change_mobile_page(self):
        self.change_mobile_window = ChangeMobilePage(self)

class ChangePasswordPage(tk.Toplevel):
    def __init__(self, parent):
        super().__init__(parent)
        self.title("Change Password")
        self.configure(bg="#ADD8E6")

        self.current_password_label = tk.Label(self, text="Current Password:", font=("Helvetica", 12), bg="#ADD8E6")
        self.current_password_label.pack(pady=(10, 0))
        self.current_password_entry = tk.Entry(self, show="*", font=("Helvetica", 12))
        self.current_password_entry.pack()

        self.new_password_label = tk.Label(self, text="New Password:", font=("Helvetica", 12), bg="#ADD8E6")
        self.new_password_label.pack(pady=(10, 0))
        self.new_password_entry = tk.Entry(self, show="*", font=("Helvetica", 12))
        self.new_password_entry.pack()

        self.save_password_button = tk.Button(self, text="Save Password", command=self.save_password, bg="#4CAF50", fg="white", font=("Helvetica", 12))
        self.save_password_button.pack(pady=5)

    def save_password(self):
        current_password = self.current_password_entry.get()
        new_password = self.new_password_entry.get()
        if current_password == "current_password":
            if new_password:
                messagebox.showinfo("Password Change", "Password changed successfully to: " + new_password)
                self.destroy()
            else:
                messagebox.showerror("Password Change", "Please enter a new password.")
        else:
            messagebox.showerror("Password Change", "Incorrect current password.")

class ChangeEmailPage(tk.Toplevel):
    def __init__(self, parent):
        super().__init__(parent)
        self.title("Change Email")
        self.configure(bg="#ADD8E6")

        self.new_email_label = tk.Label(self, text="New Email:", font=("Helvetica", 12), bg="#ADD8E6")
        self.new_email_label.pack(pady=(10, 0))
        self.new_email_entry = tk.Entry(self, font=("Helvetica", 12))
        self.new_email_entry.pack()

        self.save_email_button = tk.Button(self, text="Save Email", command=self.save_email, bg="#4CAF50", fg="white", font=("Helvetica", 12))
        self.save_email_button.pack(pady=5)

    def save_email(self):
        new_email = self.new_email_entry.get()
        if validate_email(new_email):
            messagebox.showinfo("Email Change", "Email changed successfully to: " + new_email)
            self.destroy()
        else:
            messagebox.showerror("Email Change", "Please enter a valid email.")

def validate_email(email):
    # Basic email validation, can be improved as per requirements
    if "@gmail.com" in email:
        return True
    return False

class ChangeMobilePage(tk.Toplevel):
    def __init__(self, parent):
        super().__init__(parent)
        self.title("Change Mobile Number")
        self.configure(bg="#ADD8E6")

        self.new_mobile_label = tk.Label(self, text="New Mobile Number:", font=("Helvetica", 12), bg="#ADD8E6")
        self.new_mobile_label.pack(pady=(10, 0))
        self.new_mobile_entry = tk.Entry(self, font=("Helvetica", 12))
        self.new_mobile_entry.pack()

        self.save_mobile_button = tk.Button(self, text="Save Mobile Number", command=self.save_mobile_number, bg="#4CAF50", fg="white", font=("Helvetica", 12))
        self.save_mobile_button.pack(pady=5)

    def save_mobile_number(self):
        new_mobile_number = self.new_mobile_entry.get()
        if validate_phone(new_mobile_number):
            messagebox.showinfo("Mobile Number Change", "Mobile number changed successfully to: " + new_mobile_number)
            self.destroy()
        else:
            messagebox.showerror("Mobile Number Change", "Please enter a valid mobile number.")

def validate_phone(phone):
    # Basic phone number validation, can be improved as per requirements
    if len(phone) == 10 and phone.replace("-", "").isdigit():
        return True
    return False

if __name__ == "__main__":
    app = ProfilePage()
    app.mainloop()
