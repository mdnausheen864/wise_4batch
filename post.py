import tkinter as tk
from tkinter import filedialog
from tkinter import messagebox
import os

def post_text():
    text = entry.get()
    if text:
        # Append the text post to the list
        posts.append({"type": "text", "content": text})
        # Update the text in the text box to show the new post
        update_posts()

def post_media():
    if image_path:
        # Append the media post to the list
        posts.append({"type": "media", "content": image_path})
        # Update the text in the text box to show the new post
        update_posts()

def upload_image():
    global image_path
    file_path = filedialog.askopenfilename()
    if file_path:
        image_path = file_path
        # Get the image name from the file path
        image_name = os.path.basename(image_path)
        # Display the uploaded image in a label
        display_image(image_path)

def display_image(image_path):
    try:
        # Open the image file using PhotoImage
        image = tk.PhotoImage(file=image_path)
        # Resize the image using subsample
        image = image.subsample(2, 2)  # Adjust the subsample factor as needed
        image_label.config(image=image)
        image_label.image = image  # Keep a reference to avoid garbage collection
    except tk.TclError:
        messagebox.showerror("Error", "Failed to display the image. Please try again.")

def update_posts():
    text_box.config(state=tk.NORMAL)
    text_box.delete("1.0", tk.END)
    for post in posts:
        if post['type'] == 'text':
            text_box.insert(tk.END, f"{post['content']}\n\n")
        elif post['type'] == 'media':
            # Display the image in the text box
            display_image(post['content'])
            text_box.image_create(tk.END, image=image_label.cget("image"))
            text_box.insert(tk.END, "\n\n")  # Add spacing after the image
    text_box.config(state=tk.DISABLED)

# Create a window
window = tk.Tk()
window.title("Social Media Posting")

# Set background color for the window
window.configure(bg='#D6EAF8')  # soft pastel blue

# Create a frame to contain the content with a vertical scrollbar
frame = tk.Frame(window, bg='#D6EAF8')  # soft pastel blue
frame.pack(fill=tk.BOTH, expand=True)

# Add a canvas widget to the frame
canvas = tk.Canvas(frame, bg='#D6EAF8')  # soft pastel blue
canvas.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)

# Add a scrollbar to the canvas
scrollbar = tk.Scrollbar(frame, orient=tk.VERTICAL, command=canvas.yview, bg='#D6EAF8')  # soft pastel blue
scrollbar.pack(side=tk.RIGHT, fill=tk.Y)

# Configure the canvas to use the scrollbar
canvas.configure(yscrollcommand=scrollbar.set)
canvas.bind('<Configure>', lambda e: canvas.configure(scrollregion=canvas.bbox("all")))

# Create another frame inside the canvas to contain the widgets
scrollable_frame = tk.Frame(canvas, bg='#D6EAF8')  # soft pastel blue
canvas.create_window((0, 0), window=scrollable_frame, anchor=tk.NW)

# Entry field for posting text
entry = tk.Entry(scrollable_frame, width=50)
entry.pack(pady=10)

# Button to post text
post_text_button = tk.Button(scrollable_frame, text="Post Text", command=post_text)
post_text_button.pack(pady=20)

# Button to upload image
upload_button = tk.Button(scrollable_frame, text="Upload Image", command=upload_image)
upload_button.pack()

# Label to display uploaded image
image_label = tk.Label(scrollable_frame)
image_label.pack()

# Button to post media
post_media_button = tk.Button(scrollable_frame, text="Post Media", command=post_media)
post_media_button.pack()

# Text box to display posts
text_box = tk.Text(scrollable_frame, width=50, height=20)
text_box.pack(pady=10)

# List to store posts
posts = []

# Start the GUI
window.mainloop()
