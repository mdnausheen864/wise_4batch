import tkinter as tk
import webbrowser
import os

class LinkedInFrontPage:
    urls = {
        "Profile": r"file://C:/Users/Venu Gopala Rao/OneDrive/Desktop/Carve Your Wish/profile.py",
        "Posts": r"file://C:/Users/Venu Gopala Rao/OneDrive/Desktop/Carve Your Wish/post.py",
        "Home": r"file://C:/Users/Venu Gopala Rao/OneDrive/Desktop/Carve Your Wish/carpenter-home.py",
        "Notifications": r"file://C:/Users/Venu Gopala Rao/OneDrive/Desktop/Carve Your Wish/noti.py",
    }

    def __init__(self, master):
        self.master = master
        self.master.title("Carpenter homepage")
        self.master.geometry("800x600")
        self.master.configure(bg="#f0f0f0")

        self.create_widgets()

    def create_widgets(self):
        # Header
        header_frame = tk.Frame(self.master, bg="#0073e6", height=500)
        header_frame.pack(fill=tk.X, padx=15, pady=10)

        # Profile Icon
        self.profile_icon = tk.Label(header_frame, text="👤", font=('Arial', 30), bg="#0073e6", fg="white")
        self.profile_icon.pack(side=tk.LEFT)

        # Label with text "Carve Your Wish"
        tk.Label(header_frame, text="Carve Your Wish", font=("Arial", 25), fg="white", bg="#0073e6").pack(side=tk.LEFT, padx=10)

        # Search Bar
        search_frame = tk.Frame(header_frame, bg="#f0f0f0")
        search_frame.pack(side=tk.RIGHT, padx=15)
        tk.Entry(search_frame, width=30, font=("Arial", 15)).pack(side=tk.LEFT)
        tk.Button(search_frame, text="Search", bg="#0059b3", fg="white", font=("Arial", 13)).pack(side=tk.RIGHT)

        # Main Content
        main_content_frame = tk.Frame(self.master, bg="#f0f0f0")
        main_content_frame.pack(expand=True, fill=tk.BOTH, padx=10, pady=5)

        # Left Sidebar
        left_sidebar_frame = tk.Frame(main_content_frame, bg="#e6e6e6", width=300)
        left_sidebar_frame.pack(side=tk.LEFT, fill=tk.Y, padx=5, pady=5)

        sidebar_buttons = ["Home", "Profile", "Posts", "Notifications"]
        for button_text in sidebar_buttons:
            tk.Button(left_sidebar_frame, text=button_text, font=("Arial", 13), bg="#e6e6e6", command=lambda text=button_text: self.open_webpage(text)).pack(pady=5, anchor="w")

        # Main Feed
        main_feed_frame = tk.Frame(main_content_frame, bg="#f0f0f0")
        main_feed_frame.pack(side=tk.LEFT, expand=True, fill=tk.BOTH, padx=5, pady=5)

        tk.Label(main_feed_frame, text="Carpenter Feed", font=("Arial", 16)).pack(pady=10)
        for i in range(10):
            post_frame = tk.Frame(main_feed_frame, bg="white", padx=10, pady=5, bd=1, relief="solid")
            post_frame.pack(pady=5, fill=tk.X)
            tk.Label(post_frame, text=f"User Post {i+1}", bg="white", font=("Arial", 12)).pack(anchor="w")

    def open_webpage(self, button_text):
        # Open URL corresponding to the button text in a browser
        if button_text in self.urls:
            url = self.urls[button_text]
            if url.startswith("file://"):  # Check if the URL is a local file path
                filename = url.replace("file://", "")  # Remove "file://" prefix
                filename = os.path.abspath(filename)  # Get the absolute path of the file
                try:
                    exec(open(filename).read(), globals())  # Execute the file content
                except FileNotFoundError:
                    print(f"File not found: {filename}")
            else:
                webbrowser.open(url)
        else:
            print(f"No URL defined for {button_text}")


# Now you can use these file paths in your Python script

        if button_text in urls:
            webbrowser.open_new(urls[button_text])

if __name__ == "__main__":
    root = tk.Tk()
    app = LinkedInFrontPage(root)
    root.mainloop()
