import tkinter as tk
from tkinter import messagebox
import mysql.connector
import webbrowser
import os
import subprocess

class CarveYourWishApp:
    def __init__(self, master):
        self.master = master
        self.master.title("Carve Your Wish")
        self.master.configure(bg="#ADD8E6")  # Setting background color for the entire window

        # Get the screen width and height
        screen_width = self.master.winfo_screenwidth()
        screen_height = self.master.winfo_screenheight()

        # Create frames for login and signup pages
        self.login_frame = tk.Frame(self.master, bg="#F0F0F0", bd=5, relief=tk.GROOVE)  # Light gray background
        self.signup_frame = tk.Frame(self.master, bg="#F0F0F0", bd=5, relief=tk.GROOVE)  # Light gray background

        self.create_database_connection()
        self.create_widgets()

    def create_widgets(self):
        self.user_type = tk.StringVar(value="Carpenter")

        # Radio buttons for selecting user type
        tk.Radiobutton(self.login_frame, text="Carpenter", variable=self.user_type, value="Carpenter", bg="#F0F0F0").grid(row=0, column=0, pady=5, padx=20, sticky="w")
        tk.Radiobutton(self.login_frame, text="User", variable=self.user_type, value="User", bg="#F0F0F0").grid(row=0, column=1, pady=5, padx=20, sticky="w")

        # Login Page
        self.login_frame.pack(padx=200, pady=100)

        self.login_label = tk.Label(self.login_frame, text="Login", font=("Helvetica", 16, "bold"))
        self.login_label.grid(row=1, column=0, pady=5, columnspan=2)

        self.username_label = tk.Label(self.login_frame, text="Username/Email:")
        self.username_label.grid(row=2, column=0, padx=50, pady=20)
        self.username_entry = tk.Entry(self.login_frame, bg="#E0E0E0", width=30)  # Light gray background
        self.username_entry.grid(row=2, column=1, padx=50, pady=20)

        self.password_label = tk.Label(self.login_frame, text="Password:")
        self.password_label.grid(row=3, column=0, pady=10)
        self.password_entry = tk.Entry(self.login_frame, show="*", bg="#E0E0E0", width=30)  # Light gray background
        self.password_entry.grid(row=3, column=1, pady=10)

        self.login_button = tk.Button(self.login_frame, text="Login", command=self.login, bg="#4CAF50", fg="#FFFFFF")  # Green button with white text
        self.login_button.grid(row=4, column=1, padx=100, pady=10, sticky="nsew")
        
        self.signup_carpenter_button = tk.Button(self.login_frame, text="Signup as Carpenter", command=self.show_carpenter_signup_window, bg="#FFD700", fg="#000000")
        self.signup_carpenter_button.grid(row=5, column=0, columnspan=2, pady=10)  # Centered

        self.signup_user_button = tk.Button(self.login_frame, text="Signup as User", command=self.show_user_signup_window, bg="#008CBA", fg="#FFFFFF")
        self.signup_user_button.grid(row=6, column=0, columnspan=2, pady=10)  # Centered

        self.login_message = tk.Label(self.login_frame, text="", bg="#BFEFFF")
        self.login_message.grid(row=7, column=0, columnspan=2, pady=5)  # Centered

        # Signup Page
        self.signup_frame.pack(padx=100, pady=100)

        # Calculate the screen width and height
        screen_width = self.master.winfo_screenwidth()
        screen_height = self.master.winfo_screenheight()

        # Calculate the window position
        x = (screen_width - self.master.winfo_reqwidth()) // 2
        y = (screen_height - self.master.winfo_reqheight()) // 2

        # Set the window position
        self.master.geometry(f"+{x}+{y}")

    def create_database_connection(self):
        try:
            self.conn = mysql.connector.connect(
                host="localhost",
                user="root",
                password="1234",
                database="carveyourwish"
            )
            self.cursor = self.conn.cursor()
            self.cursor.execute('''CREATE TABLE IF NOT EXISTS carpenter
                                (id INT AUTO_INCREMENT PRIMARY KEY,
                                username VARCHAR(255) NOT NULL,
                                email VARCHAR(255) NOT NULL,
                                password VARCHAR(255) NOT NULL,
                                phone VARCHAR(20),
                                country VARCHAR(100),
                                region VARCHAR(100))''')
            self.cursor.execute('''CREATE TABLE IF NOT EXISTS user
                                (id INT AUTO_INCREMENT PRIMARY KEY,
                                username VARCHAR(255) NOT NULL,
                                email VARCHAR(255) NOT NULL,
                                password VARCHAR(255) NOT NULL,
                                country VARCHAR(100),
                                region VARCHAR(100))''')
            self.conn.commit()
        except mysql.connector.Error as e:
            messagebox.showerror("Database Error", f"Error connecting to database: {e}")

    def close_database_connection(self):
        try:
            self.conn.close()
        except mysql.connector.Error as e:
            messagebox.showerror("Database Error", f"Error closing database connection: {e}")


    def login(self):
        user_type = self.user_type.get()
        username = self.username_entry.get()
        password = self.password_entry.get()

        if user_type and username and password:
        # Perform login logic based on user type
            if self.check_login(user_type, username, password):
                messagebox.showinfo("Login", f"Logged in as {user_type}: {username}")
                self.redirect_to_url(user_type)  # Redirect based on user_type
            else:
                messagebox.showerror("Login Error", "Incorrect username or password.")
        else:
            messagebox.showerror("Login Error", "Please fill in all fields.")

    def check_login(self, user_type, username, password):
        try:
            if user_type == "Carpenter":
                self.cursor.execute("SELECT * FROM carpenter WHERE username = %s AND password = %s", (username, password))
            else:
                self.cursor.execute("SELECT * FROM user WHERE username = %s AND password = %s", (username, password))
            if self.cursor.fetchone():
                return True
            else:
                return False
        except mysql.connector.Error as e:
            messagebox.showerror("Database Error", f"Error checking login: {e}")
            return False

    def show_carpenter_signup_window(self):
        # Show the signup_frame and hide the login_frame
        self.login_frame.pack_forget()
        self.signup_frame.pack()

        tk.Label(self.signup_frame, text="Username:", bg="#C8E6C9").grid(row=0, column=0, pady=10)
        self.carpenter_username_entry = tk.Entry(self.signup_frame, bg="#E0E0E0")  # Light gray background
        self.carpenter_username_entry.grid(row=0, column=1, padx=50, pady=10, sticky="nsew")

        tk.Label(self.signup_frame, text="Email:", bg="#C8E6C9").grid(row=1, column=0, pady=10)
        self.carpenter_email_entry = tk.Entry(self.signup_frame, bg="#E0E0E0")  # Light gray background
        self.carpenter_email_entry.grid(row=1, column=1, padx=50, pady=10, sticky="nsew")

        tk.Label(self.signup_frame, text="Phone Number:", bg="#C8E6C9").grid(row=2, column=0, pady=10)
        self.carpenter_phone_entry = tk.Entry(self.signup_frame, bg="#E0E0E0")  # Light gray background
        self.carpenter_phone_entry.grid(row=2, column=1, padx=50, pady=10, sticky="nsew")

        tk.Label(self.signup_frame, text="Country:", bg="#C8E6C9").grid(row=3, column=0, pady=10)
        self.carpenter_country_entry = tk.Entry(self.signup_frame, bg="#E0E0E0")  # Light gray background
        self.carpenter_country_entry.grid(row=3, column=1, padx=50, pady=10, sticky="nsew")

        tk.Label(self.signup_frame, text="Region:", bg="#C8E6C9").grid(row=4, column=0, pady=10)
        self.carpenter_region_entry = tk.Entry(self.signup_frame, bg="#E0E0E0")  # Light gray background
        self.carpenter_region_entry.grid(row=4, column=1, padx=50, pady=10, sticky="nsew")

        tk.Label(self.signup_frame, text="Create Password:", bg="#C8E6C9").grid(row=5, column=0, pady=10)
        self.carpenter_password_entry = tk.Entry(self.signup_frame, show="*", bg="#E0E0E0")  # Light gray background
        self.carpenter_password_entry.grid(row=5, column=1, padx=50, pady=10, sticky="nsew")

        tk.Label(self.signup_frame, text="Re-enter Password:", bg="#C8E6C9").grid(row=6, column=0, pady=10)
        self.carpenter_reenter_password_entry = tk.Entry(self.signup_frame, show="*", bg="#E0E0E0")  # Light gray background
        self.carpenter_reenter_password_entry.grid(row=6, column=1, padx=50, pady=10, sticky="nsew")

        tk.Button(self.signup_frame, text="Register", command=self.register_carpenter, bg="#4CAF50", fg="#FFFFFF").grid(row=7, columnspan=2, padx=100, pady=10, sticky="nsew")

    def show_user_signup_window(self):
        # Show the signup_frame and hide the login_frame
        self.login_frame.pack_forget()
        self.signup_frame.pack()

        tk.Label(self.signup_frame, text="Username:", bg="#C8E6C9").grid(row=0, column=0, pady=10)
        self.user_username_entry = tk.Entry(self.signup_frame, bg="#E0E0E0")  # Light gray background
        self.user_username_entry.grid(row=0, column=1, padx=50, pady=10, sticky="nsew")

        tk.Label(self.signup_frame, text="Email:", bg="#C8E6C9").grid(row=1, column=0, pady=10)
        self.user_email_entry = tk.Entry(self.signup_frame, bg="#E0E0E0")  # Light gray background
        self.user_email_entry.grid(row=1, column=1, padx=50, pady=10, sticky="nsew")

        tk.Label(self.signup_frame, text="Country:", bg="#C8E6C9").grid(row=2, column=0, pady=10)
        self.user_country_entry = tk.Entry(self.signup_frame, bg="#E0E0E0")  # Light gray background
        self.user_country_entry.grid(row=2, column=1, padx=50, pady=10, sticky="nsew")

        tk.Label(self.signup_frame, text="Region:", bg="#C8E6C9").grid(row=3, column=0, pady=10)
        self.user_region_entry = tk.Entry(self.signup_frame, bg="#E0E0E0")  # Light gray background
        self.user_region_entry.grid(row=3, column=1, padx=50, pady=10, sticky="nsew")

        tk.Label(self.signup_frame, text="Create Password:", bg="#C8E6C9").grid(row=4, column=0, pady=10)
        self.user_password_entry = tk.Entry(self.signup_frame, show="*", bg="#E0E0E0")  # Light gray background
        self.user_password_entry.grid(row=4, column=1, padx=50, pady=10, sticky="nsew")

        tk.Label(self.signup_frame, text="Confirm Password:", bg="#C8E6C9").grid(row=5, column=0, pady=10)
        self.user_confirm_password_entry = tk.Entry(self.signup_frame, show="*", bg="#E0E0E0")  # Light gray background
        self.user_confirm_password_entry.grid(row=5, column=1, padx=50, pady=10, sticky="nsew")

        tk.Button(self.signup_frame, text="Register", command=self.register_user, bg="#4CAF50", fg="#FFFFFF").grid(row=6, columnspan=2, padx=100, pady=10, sticky="nsew")

    def register_carpenter(self):
        username = self.carpenter_username_entry.get()
        email = self.carpenter_email_entry.get()
        phone = self.carpenter_phone_entry.get()
        country = self.carpenter_country_entry.get()
        region = self.carpenter_region_entry.get()
        password = self.carpenter_password_entry.get()
        reenter_password = self.carpenter_reenter_password_entry.get()

        if not all([username, email, phone, country, region, password, reenter_password]):
            messagebox.showerror("Registration Error", "Please fill in all the fields.")
            return

        # Check if passwords match
        if password != reenter_password:
            messagebox.showerror("Registration Error", "Passwords do not match.")
            return

        try:
            self.cursor.execute("INSERT INTO carpenter (username, email, password, phone, country, region) VALUES (%s, %s, %s, %s, %s, %s)", (username, email, password, phone, country, region))
            self.conn.commit()
            messagebox.showinfo("Registration", "Carpenter registration successful")
            self.signup_frame.pack_forget()  # Hide the signup_frame
        except mysql.connector.Error as e:
            messagebox.showerror("Database Error", f"Error registering carpenter: {e}")

    def register_user(self):
        username = self.user_username_entry.get()
        email = self.user_email_entry.get()
        country = self.user_country_entry.get()
        region = self.user_region_entry.get()
        password = self.user_password_entry.get()
        confirm_password = self.user_confirm_password_entry.get()

        if not all([username, email, country, region, password, confirm_password]):
            messagebox.showerror("Registration Error", "Please fill in all the fields.")
            return

        # Check if passwords match
        if password != confirm_password:
            messagebox.showerror("Registration Error", "Passwords do not match.")
            return

        try:
            self.cursor.execute("INSERT INTO user (username, email, password, country, region) VALUES (%s, %s, %s, %s, %s)", (username, email, password, country, region))
            self.conn.commit()
            messagebox.showinfo("Registration", "User registration successful")
            self.signup_frame.pack_forget()  # Hide the signup_frame
        except mysql.connector.Error as e:
            messagebox.showerror("Database Error", f"Error registering user: {e}")

 
    def redirect_to_url(self, url_key):
        urls = {
        "Carpenter": "carpenter-home.py",
        "User": "user-home.py"
    }

        if url_key in urls:
            url = urls[url_key]
            if os.path.exists(url):
                try:
                    subprocess.Popen(["python", url])
                except OSError as e:
                    messagebox.showerror("Execution Error", f"Error executing {url}: {e}")
            else:
                messagebox.showerror("URL Error", f"File not found: {url}")
        else:
            messagebox.showerror("URL Error", f"No URL defined for {url_key}")



if __name__ == "__main__":
    root = tk.Tk()
    app = CarveYourWishApp(root)
    root.mainloop()